*** Settings ***
Library             PuppeteerLibrary
Library             XML
Library             Dialogs

Test Teardown       Close Browser


*** Variables ***
${BASE_URL}     https://www.saucedemo.com/
${BROWSER}      chromium
${HEADLESS}     True


*** Test Cases ***
Purchase 5 items from Sauce Labs
    &{options} =    create dictionary    browser=${BROWSER}    headless=${HEADLESS}
    Open browser    ${BASE_URL}    options=${options}

    #Login
    Input text    id:user-name    standard_user
    Input text    id:password    secret_sauce
    Click Element    id:login-button
    Wait Until Element Contains    class:title    Products

    #Add items to cart
    Click Button    id:add-to-cart-sauce-labs-backpack
    Wait Until Element Is Visible    id:remove-sauce-labs-backpack
    Element Should Contain    id:remove-sauce-labs-backpack    REMOVE

    Click Button    id:add-to-cart-sauce-labs-bike-light
    Wait Until Element Is Visible    id:remove-sauce-labs-bike-light
    Element Should Contain    id:remove-sauce-labs-bike-light    REMOVE

    Click Button    id:add-to-cart-sauce-labs-bolt-t-shirt
    Wait Until Element Is Visible    id:remove-sauce-labs-bolt-t-shirt
    Element Should Contain    id:remove-sauce-labs-bolt-t-shirt    REMOVE

    Click Button    id:add-to-cart-sauce-labs-fleece-jacket
    Wait Until Element Is Visible    id:remove-sauce-labs-fleece-jacket
    Element Should Contain    id:remove-sauce-labs-fleece-jacket    REMOVE

    Click Button    id:add-to-cart-sauce-labs-onesie
    Wait Until Element Is Visible    id:remove-sauce-labs-onesie
    Element Should Contain    id:remove-sauce-labs-onesie    REMOVE

    #Checkout
    Click Element    id:shopping_cart_container
    Wait Until Element Is Visible    id:checkout

    Click Button    id:checkout
    Wait Until Element Is Visible    id:continue

    Input text    id:first-name    Daniel
    Input text    id:last-name    Stewart
    Input text    id:postal-code    14223
    Click Element    id:continue
    Wait Until Element Is Visible    id:finish

    Click Button    id:finish
    Wait Until Element Is Visible    id:checkout_complete_container
    Element Should Contain    id:checkout_complete_container    THANK YOU FOR YOUR ORDER

    # END
    Run Async Keywords
    Wait Until Element Is Visible    id:react-burger-menu-btn
    Click Element    id:react-burger-menu-btn
    Wait Until Element Is Visible    id:logout_sidebar_link
    Click Element    id:logout_sidebar_link    AND
    Wait Until Element Is Visible    id:login-button
